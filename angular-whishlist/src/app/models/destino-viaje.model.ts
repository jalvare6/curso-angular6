import {v4 as uuid} from 'uuid';

export class DestinoViaje{

	selected : boolean;
	servicios: string[];
	id= uuid();
	constructor(public nombre:string,public u:string,b: boolean,public votes: number=0){
		this.servicios=['pileta','desayuno'];
		this.selected=false;
		this.selected=b;
	}
	
	
	isSelected():boolean{
		return this.selected;
	}

	voteUp(){
		this.votes++;
	}

	voteDown(){
		this.votes--;
	}
}