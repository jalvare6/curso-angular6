import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-whishlist';
  translate :TranslateService;
  time = new Observable(observer =>{
    setInterval(()=>observer.next(new Date().toString()),1000);
  });
  
  constructor( translateParam: TranslateService) {
    console.log('***************** get translation');
    this.translate=translateParam;
    this.translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    this.translate.setDefaultLang('es');
  }


}
