import { Component, OnInit, Output,EventEmitter} from '@angular/core';
import { DestinoViaje} from './../../models/destino-viaje.model';
import { DestinosApiClient} from './../../models/destinos-api-client-model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from '../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]

})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded :EventEmitter<DestinoViaje>;
  updates : string[];
  all;
  destinosApiClientLocal : DestinosApiClient;

  constructor(private destinosApiClient: DestinosApiClient, private store : Store<AppState>) { 
    this.onItemAdded=new EventEmitter();
    this.updates=[];
    this.destinosApiClientLocal=this.destinosApiClient;
    this.store.select(state => state.destinos.favorito)
      .subscribe(d =>{
        if(d!=null){
          this.updates.push('se ha elegido a '+ d.nombre);
        }
      });
      store.select(state => state.destinos.items).subscribe(items => this.all =items);
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje){
    this.destinosApiClientLocal.add(d);
    this.onItemAdded.emit(d);

  }

  elegido(d : DestinoViaje){
    this.destinosApiClientLocal.elegir(d);
  }

  getAll(){

  }
}
