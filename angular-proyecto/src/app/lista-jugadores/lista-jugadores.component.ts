import { Component, OnInit } from '@angular/core';
import { Jugador} from './../models/jugador.model';

@Component({
  selector: 'app-lista-jugadores',
  templateUrl: './lista-jugadores.component.html',
  styleUrls: ['./lista-jugadores.component.css']
})
export class ListaJugadoresComponent implements OnInit {
  jugadores : Jugador[];

  constructor() { 
    this.jugadores=[];
    }
  ngOnInit(): void {
  }
  guardar(nombre:string, club:string, posicion:string, dorsal:string,url:string ): boolean{

    if(club=='1'){
      this.jugadores.push(new Jugador(nombre,"Arsenal",posicion,dorsal,url,'https://tmssl.akamaized.net//images/wappen/head/11.png?lm=1489787850'));
    }
    if(club=='2'){
      this.jugadores.push(new Jugador(nombre,"Manchester City",posicion,dorsal,url,'https://tmssl.akamaized.net//images/wappen/head/281.png?lm=1467356331'));
    }
    console.log(this.jugadores);
	  return false;
  }

}
