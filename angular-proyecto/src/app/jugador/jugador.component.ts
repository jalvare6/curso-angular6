import { Component, OnInit, Input ,HostBinding} from '@angular/core';
import { Jugador} from './../models/jugador.model';

@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.component.html',
  styleUrls: ['./jugador.component.css']
})
export class JugadorComponent implements OnInit {

  @Input() jugador : Jugador;
  @HostBinding('attr.class') cssClass= "col-md-4";

  constructor() { }

  ngOnInit(): void {
  }

}
