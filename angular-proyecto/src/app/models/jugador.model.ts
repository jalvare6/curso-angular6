export class Jugador{
	
	nombre:string;
	posicion:string;
	dorsal:string;
	club:string;
	url:string;
	escudo:string;

	constructor(n:string, c:string, p:string, d:string,u:string,e:string){
		this.nombre=n;
		this.club=c;
		this.posicion=p;
		this.dorsal=d;
		this.url=u;
		this.escudo=e;
	}
}